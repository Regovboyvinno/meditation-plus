import { Subscription, timer } from 'rxjs';
import { Component, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MeditationService } from '../meditation.service';
import * as moment from 'moment';
import { filter, tap, concatMap } from 'rxjs/operators';
import { MEDITATION_CHART_OPTIONS } from './meditation-chart-options';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { Logout } from '../../auth/actions/auth.actions';

/**
 * Component for the meditation tab inside home.
 */
@Component({
  selector: 'meditation-chart',
  templateUrl: './meditation-chart.component.html',
  styleUrls: [
    './meditation-chart.component.styl'
  ]
})
export class MeditationChartComponent implements OnDestroy {

  @ViewChild('chart') chart: any;

  // chart details
  chartSubscribtion: Subscription;
  chartLastHour = '';
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = MEDITATION_CHART_OPTIONS;
  loading = false;

  constructor(
    private changeDetection: ChangeDetectorRef,
    private meditationService: MeditationService,
    private store: Store<AppState>
  ) {
    // Two datasets are needed to have different colors for the bars.
    // The current hour should have other color.
    const data = {
      data: [],
      label: 'Meditation minutes',
      backgroundColor: 'rgba(255, 33, 81, 0.4)'
    };
    const dataCurrentHour = {
      data: [],
      label: 'Meditation minutes (current hour)',
      backgroundColor: 'rgba(255, 33, 81, 0.9)'
    };

    for (let i = 0; i < 24; i++) {
      this.chartData.labels.push('' + i);
    }
    this.chartData.datasets.push(data);

    // check for hour change every 10 seconds
    this.chartSubscribtion = timer(0, 10000).pipe(
      filter(() => !this.loading && moment.utc().format('H').toString() !== this.chartLastHour),
      tap(() => this.loading = true),
      concatMap(() => this.meditationService.getChartData()),
    ).subscribe(res => {
      const currentHour = moment.utc().format('H').toString();
      this.loading = false;
      this.chartLastHour = currentHour;

      data.data = [];
      dataCurrentHour.data = [];

      // fill all hours
      let values = res ? res : [];
      for (let i = 0; i < 24; i++) {
        let currentVal = 0;

        while (values.length > 0 && values[0]._id <= i) {
          // add value for current hour
          if (values[0]._id === i) {
            currentVal = values[0].total;
          }

          values = values.slice(1);
        }

        if (i === parseInt(currentHour, 10)) {
          dataCurrentHour.data.push(currentVal);
          data.data.push(0);
        } else {
          data.data.push(currentVal);
          dataCurrentHour.data.push(0);
        }
      }

      // using push() twice seems to be necessary here
      // since the second dataset won't show otherwise
      this.chartData.datasets.push(data);
      this.chartData.datasets.push(dataCurrentHour);
      this.changeDetection.detectChanges();
      this.chart.refresh();
    },
    err => {
      if (err.status === 401) {
        this.store.dispatch(new Logout());
      }
    });
  }

  ngOnDestroy() {
    this.chartSubscribtion.unsubscribe();
  }
}
