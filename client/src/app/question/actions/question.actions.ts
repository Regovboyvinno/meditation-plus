import { Action } from '@ngrx/store';
import { Question } from '../question';

export const LOAD_UNANSWERED = '[Question] Load Unanswered';
export const LOAD_UNANSWERED_DONE = '[Question] Load Unanswered Done';
export const LOAD_ANSWERED = '[Question] Load Answered';
export const LOAD_ANSWERED_DONE = '[Question] Load Answered Done';
export const POST = '[Question] Post';
export const POST_DONE = '[Question] Post Done';
export const LIKE = '[Question] Like';
export const LIKE_DONE = '[Question] Like Done';
export const ANSWERING = '[Question] Answering';
export const ANSWERING_DONE = '[Question] Answering Done';
export const CANCEL_ANSWERING = '[Question] Cancel Answering';
export const CANCEL_ANSWERING_DONE = '[Question] Cancel Answering Done';
export const ANSWER = '[Question] Answer';
export const ANSWER_DONE = '[Question] Answer Done';
export const DELETE = '[Question] Delete';
export const DELETE_DONE = '[Question] Delete Done';

export class LoadUnansweredQuestions implements Action {
  readonly type = LOAD_UNANSWERED;
}
export class LoadUnansweredQuestionsDone implements Action {
  readonly type = LOAD_UNANSWERED_DONE;
  constructor(public payload: Question[]) {}
}

export class PostQuestion implements Action {
  readonly type = POST;
  constructor(public payload: string) {}
}
export class PostQuestionDone implements Action {
  readonly type = POST_DONE;
}

export class LikeQuestion implements Action {
  readonly type = LIKE;
  constructor(public payload: Question) {}
}
export class LikeQuestionDone implements Action {
  readonly type = LIKE_DONE;
}

export class AnsweringQuestion implements Action {
  readonly type = ANSWERING;
  constructor(public payload: Question) {}
}
export class AnsweringQuestionDone implements Action {
  readonly type = ANSWERING_DONE;
}

export class CancelAnsweringQuestion implements Action {
  readonly type = CANCEL_ANSWERING;
  constructor(public payload: Question) {}
}
export class CancelAnsweringQuestionDone implements Action {
  readonly type = CANCEL_ANSWERING_DONE;
}

export class AnswerQuestion implements Action {
  readonly type = ANSWER;
  constructor(public payload: Question) {}
}
export class AnswerQuestionDone implements Action {
  readonly type = ANSWER_DONE;
}

export class DeleteQuestion implements Action {
  readonly type = DELETE;
  constructor(public payload: Question) {}
}
export class DeleteQuestionDone implements Action {
  readonly type = DELETE_DONE;
}

export interface LoadAnsweredQuestionsQuery {
  sortBy: string;
  sortOrder: 'descending' | 'ascending' ;
  linkOnly: boolean;
  search: string;
}
export const loadAnsweredQuestionInitialQuery: LoadAnsweredQuestionsQuery = {
  sortBy: 'answeredAt',
  sortOrder: 'descending',
  linkOnly: false,
  search: ''
};

export interface LoadAnsweredQuestionsPayload {
  query: LoadAnsweredQuestionsQuery;
  intention: 'search' | 'more';
}
export class LoadAnsweredQuestions implements Action {
  readonly type = LOAD_ANSWERED;
  constructor(public payload: LoadAnsweredQuestionsPayload) {}
}
export interface LoadAnsweredQuestionsDonePayload extends LoadAnsweredQuestionsPayload {
  questions: Question[];
}
export class LoadAnsweredQuestionsDone implements Action {
  readonly type = LOAD_ANSWERED_DONE;
  constructor(public payload: LoadAnsweredQuestionsDonePayload) {}
}

export type Actions = LoadUnansweredQuestions
 | LoadUnansweredQuestionsDone
 | LoadAnsweredQuestions
 | LoadAnsweredQuestionsDone
 | LikeQuestion
 | LikeQuestionDone
 | PostQuestion
 | PostQuestionDone
 | AnsweringQuestion
 | AnsweringQuestionDone
 | CancelAnsweringQuestion
 | CancelAnsweringQuestionDone
 | AnswerQuestion
 | AnswerQuestionDone
;
