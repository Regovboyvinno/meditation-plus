import * as global from '../actions/global.actions';
import { AppState } from '.';
import { createSelector } from '@ngrx/store';

export interface GlobalState {
  title: string;
  sidenavOpen: boolean;
  lastConnectionStatus: number | null;
}

export const initialGlobalState: GlobalState = {
  title: '',
  sidenavOpen: false,
  lastConnectionStatus: null
};

export function globalReducer(
  state = initialGlobalState,
  action: global.Actions
): GlobalState {
  switch (action.type) {
    case global.SET_TITLE: {
      return { ...state, title: action.payload };
    }
    case global.SET_SIDENAV_OPEN: {
      return { ...state, sidenavOpen: action.payload };
    }
    case global.SET_CONNECTION_STATUS: {
      return { ...state, lastConnectionStatus: action.payload };
    }

    default: {
      return state;
    }
  }
}

// Selectors for easy access
export const selectGlobal = (state: AppState) => state.global;
export const selectTitle = createSelector(selectGlobal, (state: GlobalState) => state.title);
export const selectSidenavOpen = createSelector(selectGlobal, (state: GlobalState) => state.sidenavOpen);
export const selectLastConnectionStatus = createSelector(selectGlobal, (state: GlobalState) => state.lastConnectionStatus);
export const selectConnectionProblems = createSelector(
  selectGlobal, (state: GlobalState) => state.lastConnectionStatus === 503 || state.lastConnectionStatus === 0
);

