import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { WebsocketOnConnectPayload } from 'app/message/actions/message.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectToken } from '../auth/reducders/auth.reducers';
import { take, filter, tap, map, concatMap } from 'rxjs/operators';

@Injectable()
export class WebsocketService {
  private socket: SocketIOClient.Socket;

  public constructor(private store: Store<AppState>) {

  }

  /**
   * Initializes Socket.io client with Jwt.
   */
  public getSocket(): Observable<SocketIOClient.Socket> {
    return this.store.select(selectToken).pipe(
      take(1),
      filter(val => !!val),
      tap(val => {
        if (typeof this.socket === 'undefined') {
          this.socket = io(ApiConfig.url, {
            transports: ['websocket'],
            query: 'token=' + val
          });
        }
      }),
      map(() => this.socket)
    );
  }

  public onConnected(): Observable<WebsocketOnConnectPayload> {
    return this.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('connection', res => obs.next(res));
      }))
    );
  }

  /**
   * Event that gets triggered when any user sends a new chat message
   */
  public onMessage(): Observable<any> {
    return this.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('message', res => obs.next(res));
      }))
    );
  }

  /**
   * Disconnects socket
   */
  public disconnect(): void {
    this.socket.disconnect();
  }
}
