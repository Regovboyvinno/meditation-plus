import { Component, ViewChild, OnInit } from '@angular/core';
import { MeditationComponent } from '../meditation';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user/user.service';
import { MessageService } from '../message/message.service';
import { QuestionService } from '../question/question.service';
import { WebsocketService, SettingsService } from '../shared';
import * as moment from 'moment';
import { filter, map, concatMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle, SetSidenavOpen } from '../actions/global.actions';
import { Observable, timer } from 'rxjs';
import { MatTabGroup } from '@angular/material';
import { MessageComponent } from '../message';
import { selectIsMeditating } from 'app/meditation/reducers/meditation.reducers';
import * as firebase from 'firebase/app';
import 'firebase/messaging';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.styl'
  ]
})
export class HomeComponent implements OnInit {
  @ViewChild(MeditationComponent) medComponent: MeditationComponent;
  @ViewChild(MessageComponent) messageComponent: MessageComponent;
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;

  currentTab = '';
  activated: string[] = [];
  isMeditating$: Observable<boolean>;
  newMessages = 0;
  newQuestions = 0;
  notification$: Observable<string>;

  tabs = ['meditation', 'chat', 'ask'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private messageService: MessageService,
    private questionService: QuestionService,
    private wsService: WebsocketService,
    private store: Store<AppState>,
    private settings: SettingsService
  ) {
    this.store.dispatch(new SetTitle(''));
    this.store.dispatch(new SetSidenavOpen(false));

    this.isMeditating$ = this.store.select(selectIsMeditating);

    this.notification$ = timer(0, 10000).pipe(
      concatMap(() => this.settings.get()),
      map(val => val.notification)
    );
  }

  navigate(index: number) {
    this.router.navigate(['home', {tab: this.tabs[index]}]);
    this.tab(this.tabs[index]);
  }

  getButtonColor(tab: string) {
    return this.currentTab === tab ? 'primary' : '';
  }

  isCurrentTab(tab: string): boolean {
    return this.currentTab === tab;
  }

  tab(tab: string) {
    const index = this.tabs.indexOf(tab);
    this.tabGroup.selectedIndex = index > -1 ? index : 0;
    this.currentTab = tab;

    if (tab === 'chat') {
      this.newMessages = 0;
      this.messageService.setLastMessage(moment().toISOString());

      if (this.messageComponent) {
        this.messageComponent.scrollToBottom();
      }
    }

    if (this.activated.indexOf(tab) < 0) {
      this.activated.push(tab);
    }
  }

  openSidenav() {
    this.store.dispatch(new SetSidenavOpen(true));
  }

  /**
   * Loads content with lower priority
   */
  afterLoadedComponent() {
    const lastMessageDate = this.messageService.getLastMessage();
    if (lastMessageDate) {
      // Get number of new messages
      this.messageService.synchronize(moment(lastMessageDate).utc(), moment().utc(), true)
        .subscribe(res => this.newMessages = res);
    }

    // Update message counter on new message
    this.wsService.onMessage()
      .pipe(
        filter(() => this.currentTab !== 'chat')
      )
      .subscribe(() => this.newMessages++);

    // Get number of new questions
    this.questionService.getCount()
      .subscribe(count => this.newQuestions = count);

    // Update question counter on new question
    this.questionService.getSocket()
      .subscribe(res => !(res < 0 && this.newQuestions < 1) ? this.newQuestions += res : null);
  }

  ngOnInit() {
    this.userService.initializeFirebase();

    // set initial tab
    this.tab(this.route.snapshot.params['tab'] || 'meditation');

    if (firebase.apps.length) {
      const messaging = firebase.messaging();

      messaging.requestPermission().then(() => messaging.getToken().then(token => {
        localStorage.setItem('MEDITATION_PLUS_FCM_TOKEN', token);
        this.userService
          .registerPushSubscription(token)
          .subscribe();
      }));

      messaging.onTokenRefresh(() => messaging.getToken().then(token => {
        const oldToken = localStorage.getItem('MEDITATION_PLUS_FCM_TOKEN');
        localStorage.setItem('MEDITATION_PLUS_FCM_TOKEN', token);
        this.userService
          .registerPushSubscription(token, oldToken)
          .subscribe();
      }));
    }
  }
}
