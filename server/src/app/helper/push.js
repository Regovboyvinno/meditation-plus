import { User } from '../models/user.model.js';
import FCMSubscription from '../models/fcm.model.js';
import firebase from 'firebase-admin';
import { logger } from './logger.js';

const push = {
  /**
   * Send a Push message out to a user.
   *
   * @param  username   either username or query to find user(s)
   * @param  data       payload for notification
   */
  send: async (userquery, message) => {
    if (!userquery || !message) {
      return;
    }
    
    try {
      const users = await User.find(userquery);

      if (!users) {
        return;
      }

      const subscriptions = await FCMSubscription.find({
        user: { $in: users.map(u => u._id) }
      });

      if (!subscriptions) {
        return;
      }

      subscriptions.map(({ token }) => firebase.messaging()
        .send({
          ...message,
          token
        })
        .then(stats => logger.info('FCM SENT', stats))
        .catch(error => logger.error('FCM SENDING ERROR:', error))
      );
    } catch (err) {
      logger.error('PUSH ERROR', err);
    }
  }
};

export default push;

