import mongoose from 'mongoose';

let MeetingSchema = mongoose.Schema({
  user: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  initiated: { type: Boolean, default: false },
  closed: { type: Boolean, default: false },
  token: String
}, {
  timestamps: true
});

export default mongoose.model('AppointmentMeeting', MeetingSchema);
