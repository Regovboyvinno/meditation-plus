import mongoose from 'mongoose';

let fcmSubscriptionsSchema = mongoose.Schema({
  user : { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  token: { type: String, required: true }
}, {
  timestamps: true
});

export default mongoose.model('FCMSubscription', fcmSubscriptionsSchema);
